<div align="center">
    <img src="https://brandslogos.com/wp-content/uploads/images/angular-logo-vector-1.svg" width="200">
</div>

# Angular - Retrieve FB Live Comments

Retrieve comments from Facebook Live with Profile Facebook

## Table of Contents

- [Prerequisites](#prerequisites)
- [Configuration](#configuration)
- [Installation](#installation)
- [Usage](#usage)
- [Credits](#credits)
- [License](#license)

## Prerequisites

* Angular CLI : 14.2.5
* Node : 14.18.1
* Package Manager (npm) : 6.14.15

## Configuration

* Port : 4200

## Installation

Using npm:

```bash
npm install
```

Using docker:

```bash
docker compose up -d
```

## Usage

Using npm:

```bash
npm run start:dev
```

Using docker:

```bash
docker ps --filter name=angular_retrieve_fb_live_comments
```

> Open your browser and navigate to [http://localhost:4200](http://localhost:4200)

## Credits

Itsara Rakchanthuek

## License

[MIT](LICENSE)