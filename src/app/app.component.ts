import { Component } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SocialAuthService, SocialLoginModule, SocialUser, FacebookLoginProvider } from '@abacritt/angularx-social-login';
import Swal from 'sweetalert2';
import * as moment from 'moment';

export interface LiveVideo {
  id?: string,
  status?: string,
  embed_html?: string,
  title?: string
}

export interface Comments {
  id?: string,
  name?: string,
  email?: string,
  message?: string,
  comment_id?: string,
  created_time?: string,
}

export interface Orders {
  id?: string,
  name?: string,
  email?: string,
  product: any,
  cf?: number,
  total?: number,
  created_time?: string,
}

export interface Product {
  id?: number,
  name?: string,
  price?: number,
  stock?: number
}

export interface ProductLive {
  id?: number,
  name?: string,
  price?: number,
  stock?: number,
  cf?: number,
  condition?: string
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  loginForm!: FormGroup;
  socialUser!: SocialUser;
  isLoggedin?: boolean = false;
  apiPath: string = '';
  LiveVideo: LiveVideo = {};
  comments: Comments[] = [];
  orders: Orders[] = [];
  product: Product[] = [];
  productTemp: Product[] = [];
  productLive: ProductLive[] = [];
  cfWord: string = 'cf';
  selectedProductTemp: number = 0;
  ordersStatus: boolean = false;
  getLiveVideoCommentsInterval: any;
  liveStatus: boolean = false;

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private socialAuthService: SocialAuthService
  ) { }

  ngOnInit() {
    this.product = [
      {
        id: 1,
        name: 'เสื้อยืดสีพื้น',
        price: 80,
        stock: 100
      },
      {
        id: 2,
        name: 'กางเกงขาสั้น',
        price: 120,
        stock: 100
      },
      {
        id: 3,
        name: 'หมวกแก๊ป',
        price: 180,
        stock: 100
      },
      {
        id: 4,
        name: 'นาฬิกา',
        price: 990,
        stock: 100
      }];
    this.productTemp = [...this.product];

    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      this.isLoggedin = (user != null);
      console.log('socialUser', this.socialUser);
    });
  }

  loginWithFacebook(): void {
    const fbLoginOptions = { scope: 'email, user_photos, user_videos' };
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID, fbLoginOptions);
  }

  setLiveDefault() {
    clearInterval(this.getLiveVideoCommentsInterval);
    this.comments = [];
    this.productLive = [];
    this.productTemp = [...this.product];
    this.orders = [];
    this.liveStatus = false;
    this.ordersStatus = false;
    this.selectedProductTemp = 0;
  }

  getLiveVideo() {
    if (this.productLive.length) {
      this.apiPath = `https://graph.facebook.com/v17.0/${this.socialUser['id']}/live_videos?access_token=${this.socialUser['authToken']}`;
      this.http.get(this.apiPath).subscribe((res: any) => {
        console.log('res', res)
        if (res.data.length && (res.data[0]['status'] === 'LIVE')) {
          this.liveStatus = true;
          this.LiveVideo = res.data[0];
          this.getLiveVideoCommentsInterval = setInterval(() => this.getLiveVideoComments(), 5000);
          console.log('LiveVideo', this.LiveVideo);
          console.log('LiveVideo', this.LiveVideo['embed_html']);
        }
      });

      Swal.fire({
        title: 'กำลังตรวจสอบ กรุณารอสักครู่',
        timer: 6000,
        timerProgressBar: true,
        allowOutsideClick: false,
        didOpen: () => Swal.showLoading()
      }).then(() => {
        this.http.get(this.apiPath).subscribe((res: any) => {
          if (!this.liveStatus) {
            Swal.fire({
              icon: 'warning',
              title: 'ไม่พบ LIVE',
              text: 'กรุณาเปิด FB Live แล้วลองอีกครั้ง'
            });
          }
        });
      });
    } else {
      Swal.fire({
        icon: 'warning',
        title: 'ไม่พบสินค้าใน FB Live',
        text: 'กรุณาเลือกสินค้าอย่างน้อย 1 รายการแล้วลองอีกครั้ง'
      });
    }
  }

  getLiveVideoComments() {
    this.apiPath = `https://graph.facebook.com/v17.0/${this.LiveVideo['id']}/comments?access_token=${this.socialUser['authToken']}`;
    this.http.get(this.apiPath).subscribe((res: any) => {
      if (res.data.length) {
        if (!this.comments.length) {
          res.data.forEach((dataRes: any) => {
            this.comments.unshift({
              id: dataRes['from']['id'],
              name: dataRes['from']['name'],
              email: dataRes['from']['email'],
              message: dataRes['message'],
              comment_id: dataRes['id'],
              created_time: moment(dataRes['created_time']).format('YYYY-MM-DD HH:mm:ss')
            });
          });
        } else {
          res.data.forEach((dataRes: any) => {
            if (!this.comments.map((dataComments) => dataComments['comment_id']).includes(dataRes['id'])) {
              this.comments.unshift({
                id: dataRes['from']['id'],
                name: dataRes['from']['name'],
                email: dataRes['from']['email'],
                message: dataRes['message'],
                comment_id: dataRes['id'],
                created_time: moment(dataRes['created_time']).format('YYYY-MM-DD HH:mm:ss')
              });
            }
          });
        }
      }

      this.getOrders();
      console.log('comments', this.comments);
    });
  }

  manageProductLive(type: string = '', productId: number = 0) {
    this.selectedProductTemp = productId;
    console.log('productId', productId);

    switch (type) {
      case 'add':
        if (productId != 0) {
          Swal.fire({
            title: 'กรอกรหัส CF สำหรับให้ลูกค้าจองสินค้า',
            text: 'เช่น กรอก A1 (ถ้าลูกค้าพิมพ์ A1 5 หมายถึงลูกค้าสั่งสินค้ารหัส A1 จำนวน 5 ชิ้น)',
            input: 'text',
            width: 650,
            inputAttributes: {
              autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ยกเลิก',
            showLoaderOnConfirm: true,
            allowOutsideClick: false,
            preConfirm: (result) => {
              if (!result) return Swal.showValidationMessage('กรุณากรอกรหัส CF');
              if (result && this.productLive.map((dataProductLive) => dataProductLive['condition']).includes(result)) Swal.showValidationMessage('รหัส CF ซ้ำ กรอกรหัสแล้วลองอีกครั้ง');
            }
          }).then((result) => {
            if (result.isConfirmed && result['value']) {
              for (let [index, data] of this.productTemp.entries()) {
                if (data['id'] == productId) {
                  this.productLive.unshift({ ...data, cf: 0, condition: result['value'] });
                  this.productTemp.splice(index, 1);
                  break;
                }
              }
            }
          });
        }

        break;
      case 'remove':
        Swal.fire({
          title: 'คุณต้องการลบสินค้าใช่หรือไม่',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'ยืนยัน',
          cancelButtonText: 'ยกเลิก',
        }).then((result) => {
          if (result.isConfirmed) {
            for (let [index, data] of this.productLive.entries()) {
              if (data['id'] == productId) {
                this.productLive.splice(index, 1);

                this.product.filter((dataProduct) => {
                  if (dataProduct['id'] == productId) this.productTemp.push(dataProduct);
                });

                break;
              }
            }
          }
        });

        break;
    }

    this.selectedProductTemp = 0;
    console.log('productLive', this.productLive);
    console.log('productTemp', this.productTemp);
    console.log('product', this.product);
  }

  getOrders() {
    this.orders = [];
    let stock: any;
    let cf: number = 0;
    let commentCondition: string = '';
    let commentCF: number = 0;

    this.productLive.forEach((dataProductLive: any) => {
      for (let dataProduct of this.product) {
        if (dataProduct['id'] === dataProductLive['id']) {
          stock = dataProduct['stock'];
          cf = 0;
          break;
        }
      }

      this.comments.forEach((dataComments: any) => {
        commentCondition = (dataComments['message'].split(' ')[0] || '');
        commentCF = (Number(dataComments['message'].split(' ')[1]) || 0);

        if (commentCondition && commentCF) {
          if (commentCondition === dataProductLive['condition']) {
            stock -= commentCF;
            cf += commentCF;
            dataProductLive['stock'] = stock;
            dataProductLive['cf'] = cf;
            this.orders.unshift({
              id: dataComments['id'],
              name: dataComments['name'],
              email: dataComments['email'],
              product: { ...dataProductLive },
              cf: commentCF,
              total: (commentCF * dataProductLive['price']),
              created_time: dataComments['created_time']
            });
          }
        }
      });
    });

    this.ordersStatus = true;
    console.log('orders', this.orders);
  }

  signOut(): void {
    this.socialAuthService.signOut();
  }
}